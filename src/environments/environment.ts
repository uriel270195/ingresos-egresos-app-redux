// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAkwJ45RgfKbcIm7twPbsXbrhFkG4RYOZ8',
    authDomain: 'ingreso-egreso-app-e3345.firebaseapp.com',
    databaseURL: 'https://ingreso-egreso-app-e3345.firebaseio.com',
    projectId: 'ingreso-egreso-app-e3345',
    storageBucket: 'ingreso-egreso-app-e3345.appspot.com',
    messagingSenderId: '73460921840',
    appId: '1:73460921840:web:a863fee1a14469b79a1935',
    measurementId: 'G-SEJT0KHGTD'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
