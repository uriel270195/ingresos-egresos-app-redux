import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { map } from 'rxjs/operators';
import { User } from './register/user.model';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _auth: AngularFireAuth, private router: Router, private _afDB: AngularFirestore) { }

  initAuthListener() {
    this._auth.authState.subscribe(fireBaseUser => {
      console.log(fireBaseUser);
    });
  }

  crearUsuario(nombre: string, email: string, password: string) {
    this._auth.auth.createUserWithEmailAndPassword(email, password).then(resp => {
      // console.log(resp);
      const user: User = {
        uid: resp.user.uid,
        email: resp.user.email,
        nombre: nombre
      };
      this._afDB.doc(`${user.uid}/usuario`).set(user).then(() => {
        this.router.navigate(['/']);
      });
    }).catch(err => {
      console.log(err);
      Swal.fire('Error en el login', err.message, 'error');
    });
  }

  login(email: string, password: string) {
    this._auth.auth.signInWithEmailAndPassword(email, password).then(resp => {
      console.log(resp);
      this.router.navigate(['/']);
    }).catch(err => {
      console.log(err);
      Swal.fire('Error en el login', err.message, 'error');
    });
  }

  logout() {
    this.router.navigate(['/login']);
    this._auth.auth.signOut();
  }

  isauth() {
    return this._auth.authState.pipe(
      map(m => {
        if (m === null) {
          this.router.navigate(['/login']);
        }
        return  m !== null;
      })
    );
  }
}
