import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit {

  constructor(private _auth:AuthService) { }

  ngOnInit() {
  }

  onSubmit(form) {
    console.log(form);
    this._auth.crearUsuario(form.name, form.email, form.pass);
  }

}
